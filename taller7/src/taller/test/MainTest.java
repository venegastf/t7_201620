package taller.test;

import java.io.IOException;

import estructuras.Arbol;
import junit.framework.TestCase;
import mundo.Generador;

public class MainTest extends TestCase{
   
  public void testSample() {
    assertEquals(2, 2);
    Generador g= new Generador();
    try {
		g.cargarArchivo("nombreyapellido.properties");
		Arbol<String> nombreYAp=g.getGenerado();
		System.out.println(nombreYAp.toString());
		g.cargarArchivo("nombre.properties");
		Arbol<String> nombre=g.getGenerado();
		System.out.println(nombre.toString());
		
		System.out.println("El arbol de nombre es sub-arbol de el de apellido y nombre: "+ nombreYAp.buscarSubArbol(nombre));
		g.cargarArchivo("apellido.properties");
		Arbol<String> apellido=g.getGenerado();
		System.out.println("El arbol de apellido es sub-arbol de el de apellido y nombre: "+ nombreYAp.buscarSubArbol(nombre));

		g.cargarArchivo("caso1.properties");
		Arbol<String> caso1=g.getGenerado();
		//
		System.out.println("\nCaso1:\n"+caso1.toString());
		
		g.cargarArchivo("caso2.properties");
		Arbol<String> caso2=g.getGenerado();
		g.reconstruir();
		System.out.println("\nCaso2:\n"+caso2.toString());
		
    
    
    } catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
    
   
  }
  
}