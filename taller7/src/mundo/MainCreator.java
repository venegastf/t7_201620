package mundo;

import java.util.ArrayList;

import estructuras.Arbol;

public class MainCreator {
	ArrayList<ArrayList<String>> order;
	public MainCreator(String nombre){
	order= PropertiesReader.read(nombre);
	}
	private  Arbol<String> createArbol(Arbol<String> raiz, ArrayList<String> inIzquierda,ArrayList<String>inDerecha ,ArrayList<String> pre ){
		
		int indexI=0;
		
		if (inIzquierda.size()<=1){
			if (inIzquierda.size()==0){}
			else raiz.agregarIzquierda(new Arbol<String>(inIzquierda.get(0)));
		}
		else{
		
		while (!inIzquierda.get(indexI).equals(pre.get(0))){
			indexI++;
		}
		ArrayList<String> izquierdaInIzquierda=new ArrayList<String>();
		ArrayList<String> derechaInIzquierda=new ArrayList<String>();
		ArrayList<String> preIzquierda = new ArrayList<String>();
		
	
		
		for (int i=0; i<indexI;i++){
			izquierdaInIzquierda.add(inIzquierda.get(i));
		}
		for (int i=indexI+1; i<inIzquierda.size();i++){
			derechaInIzquierda.add(inIzquierda.get(i));
		}
		for (int i=1; i<izquierdaInIzquierda.size()+derechaInIzquierda.size()+1;i++){ 
			preIzquierda.add(pre.get(i));
		}
		raiz.agregarIzquierda(createArbol(new Arbol<String>(inIzquierda.get(indexI)),izquierdaInIzquierda,derechaInIzquierda,preIzquierda));
		}
		if (inDerecha.size()<=1){
			if (inDerecha.size()==0) return raiz;
			raiz.agregarDerecha(new Arbol<String>(inDerecha.get(0)));
		}
		else{
		ArrayList<String> izquierdaInDerecha=new ArrayList<String>();
		ArrayList<String> derechaInDerecha=new ArrayList<String>();
		ArrayList<String> preDerecha = new ArrayList<String>(); 
		for (int i=inIzquierda.size()+1;i<pre.size();i++){
			preDerecha.add(pre.get(i));
		}
		
		int indexD=0;
		while (!inDerecha.get(indexD).equals(pre.get(inIzquierda.size()))){
			indexD++;
		}
		for (int i=0; i<indexD;i++){
			izquierdaInDerecha.add(inDerecha.get(i));
		}
		for (int i=indexD+1; i<inDerecha.size();i++){
			derechaInDerecha.add(inDerecha.get(i));
		}
		
		raiz.agregarDerecha(createArbol(new Arbol<String>(inDerecha.get(indexD)),izquierdaInDerecha,derechaInDerecha,preDerecha));
		}
		return raiz;
		}
		
		
	
	public Arbol<String> creator(){
		ArrayList<String> pre = order.get(0);
		ArrayList<String> in = order.get(1);
		Arbol<String> raiz = new Arbol<String>(pre.get(0));
		int indexD=0;
		while (!in.get(indexD).equals(pre.get(0))){
			indexD++;
		}
		ArrayList<String> izquierda = new ArrayList<String>();
		ArrayList<String> derecha = new ArrayList<String>();
		for (int i=0; i<indexD;i++){
			izquierda.add(in.get(i));
		}
		for (int i=indexD+1; i<in.size();i++){
			derecha.add(in.get(i));
		}
		pre.remove(0);
		return createArbol(raiz,izquierda,derecha,pre);
	}
}
