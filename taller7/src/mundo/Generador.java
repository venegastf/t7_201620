package mundo;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

import estructuras.Arbol;
import taller.interfaz.IReconstructorArbol;

public class Generador implements IReconstructorArbol {
	MainCreator creator;
	private Arbol<String> generado;
	public Generador(){
		creator=null;
		setGenerado(null);
	}
	@Override
	public void cargarArchivo(String nombre) throws IOException {
		creator=new MainCreator(nombre);
		setGenerado(creator.creator());
		
	}

	@Override
	public void crearArchivo(String info) throws FileNotFoundException, UnsupportedEncodingException {
		try(BufferedWriter wr= new BufferedWriter( new FileWriter( new File("./data/arbolPlantado.json")))) {
			wr.write(info);
			wr.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		
		
	}
	
	private String escribirArbol(Arbol<String> raiz){
		if(raiz==null) return "null";
		String message="{\n\"raiz\" : \""+raiz.getValue()+"\",\n"
				+ "\"izquierda\" : "+ escribirArbol(raiz.getIzquierda())+",\n"
						+ "\"derecha\" : "+ escribirArbol(raiz.getDerecha())+"\n"
								+ "}";
		return message;
	}

	@Override
	public void reconstruir() {
		String arbolJSON=escribirArbol(getGenerado());
		try {
			crearArchivo(arbolJSON);
		} catch (FileNotFoundException | UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public Arbol<String> getGenerado() {
		return generado;
	}
	public void setGenerado(Arbol<String> generado) {
		this.generado = generado;
	}
	

}
