package mundo;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class PropertiesReader {
	public static ArrayList<ArrayList<String>> read(String nombre) {
		ArrayList<ArrayList<String>> value= new ArrayList<ArrayList<String>>();
		try{
		BufferedReader br= new BufferedReader( new FileReader( new File("./data/"+nombre)));
		String line=br.readLine();
		String[] splitted=line.split("=");
		splitted=splitted[1].split(",");
		ArrayList<String> pre= new ArrayList<String>();
		for(String c : splitted){
			pre.add(c);
		}
		value.add(pre);
		// second line reading: 
		
		line=br.readLine();
		splitted=line.split("=");
		splitted=splitted[1].split(",");
		ArrayList<String> in= new ArrayList<String>();
		for(String c : splitted){
			in.add(c);
		}
		value.add(in);
		}
		catch (IOException e){
			e.printStackTrace();
		}
		return value;
	}
}
