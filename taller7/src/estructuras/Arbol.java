package estructuras;


public class Arbol<K> {

	private K key;
	
	private Arbol<K> derecha;
	
	private Arbol<K> izquierda;
	
	private int size;
	
	public Arbol(K pKey){
		key=pKey;
		derecha=null;
		izquierda=null;
		size=1;
	}
	
	public void put ( K pKey, boolean derecha){
		if (derecha){
			this.derecha= new Arbol(pKey);
		}
		else this.izquierda= new Arbol(pKey);
		size++;
		}

	public int size(){
		return size;
	}
	
	public void agregarIzquierda(Arbol<K> pIzquierda){
		izquierda=pIzquierda;
	}
	public void agregarDerecha(Arbol<K> pDerecha){
		derecha=pDerecha;
	}
	public K getValue(){
		return key;
	}
	public Arbol<K> getIzquierda(){
		return izquierda;
	}
	public Arbol<K> getDerecha(){
		return derecha;
	}
	public boolean buscarSubArbol(Arbol<K> subArbol){
		return buscarSubArbolRec(subArbol,subArbol);
	}
	private boolean buscarSubArbolRec(Arbol<K> subArbolOrig, Arbol<K> subArbol){
		if (subArbol==null) return true;
		if(key.equals(subArbol.key)){
			if (subArbol.izquierda==null && subArbol.derecha==null) return true;
			if(izquierda!=null && derecha!= null){
				 return izquierda.buscarSubArbolRec(subArbolOrig,subArbol.izquierda)&& derecha.buscarSubArbolRec(subArbolOrig,subArbol.derecha);
			}
			if (izquierda==null && derecha==null){
				if (subArbol.izquierda==null && subArbol.derecha==null) return true;
				else return false;
			}
			if(izquierda==null && subArbol.izquierda!=null) return derecha.buscarSubArbolRec(subArbolOrig,subArbolOrig);
			if (derecha==null && subArbol.derecha!=null) return izquierda.buscarSubArbolRec(subArbolOrig,subArbolOrig);
			if(izquierda==null && subArbol.izquierda==null) return derecha.buscarSubArbolRec(subArbolOrig,subArbol.derecha);
			if(derecha==null && subArbol.derecha==null) return izquierda.buscarSubArbolRec(subArbolOrig,subArbol.izquierda);

		}
		else{
			if (izquierda==null && derecha==null) return false;
			if(izquierda!=null && derecha!= null){
			return izquierda.buscarSubArbolRec(subArbolOrig,subArbol)|| derecha.buscarSubArbolRec(subArbolOrig,subArbol);
			}
			if(izquierda!=null) return izquierda.buscarSubArbolRec(subArbolOrig,subArbol);
			else return derecha.buscarSubArbolRec(subArbolOrig,subArbol);
		}
		return false;
		
	}
	@Override
	public String toString(){
		if (derecha==null && izquierda==null) return "\n"+key.toString();
		
		if(derecha!=null && izquierda!=null) return "\n"+key.toString()+"\n"+izquierda.toString()+derecha.toString();
		else {
			if(izquierda==null) return "\n"+key.toString()+"\n  "+derecha.toString();
			else return key.toString()+"\n"+izquierda.toString();
		}
	}
}
